//
//  SearchDAO.swift
//  BookSeeker
//
//  Created by Ewerton Pereira on 07/12/19.
//  Copyright © 2019 Ewerton Pereira. All rights reserved.
//
import UIKit
import CoreData

class SearchDAO {
    
    func add (term:String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Search", in: context)
        let search = NSManagedObject(entity: entity!, insertInto: context)
        
        search.setValue(term, forKey: "term")
        search.setValue(Date(), forKey: "created")
        
        do {
           try context.save()
          } catch {
           print("Failed saving")
        }
        
    }
    
    
    func list ()->[String]{
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Search")
        request.returnsObjectsAsFaults = false

        let created = NSSortDescriptor(key:"created", ascending:false)
        request.sortDescriptors = [created]
        request.fetchLimit = 20
        
        do {
            let result = try context.fetch(request)
            var terms = [String]()
            for data in result as! [NSManagedObject] {
                terms.append(data.value(forKey: "term") as! String)
            }
            return terms
        } catch {
            print("Failed")
        }
        
        return [String]()
        
    }
    
}
