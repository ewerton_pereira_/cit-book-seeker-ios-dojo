//
//  Book.swift
//  BookSeeker
//
//  Created by Ewerton Pereira on 07/12/19.
//  Copyright © 2019 Ewerton Pereira. All rights reserved.
//

struct Book: Codable {
    
    var artworkUrl60:String?
    var artworkUrl100:String?
    var trackName:String?
    var formattedPrice:String?
    var releaseDate:String?
    var artistName:String?
    var genres:[String]?
    var description:String?
    var averageUserRating:Float?
    var userRatingCount:Int?
    
}
