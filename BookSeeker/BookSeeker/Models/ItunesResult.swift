//
//  ItunesResult.swift
//  BookSeeker
//
//  Created by Ewerton Pereira on 07/12/19.
//  Copyright © 2019 Ewerton Pereira. All rights reserved.
//

struct ItunesResult:Codable {
    var resultCount:Int?
    var results:[Book]?
    
}
