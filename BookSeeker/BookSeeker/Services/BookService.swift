//
//  BookService.swift
//  BookSeeker
//
//  Created by Ewerton Pereira on 07/12/19.
//  Copyright © 2019 Ewerton Pereira. All rights reserved.
//

import Alamofire

class BookService  {

    static let baseUrl = "https://itunes.apple.com/search"
    static var msg = "There was a problem with our servers, please try again."

    static var headers  = [
        "Content-Type": "application/json",
        "Accept": "application/json",
    ]

    static func searchBookWithTerm ( _ term:String, completion: @escaping (_ success:Bool, _ msg:String, _ books:[Book]?) -> Void){
          
        let url =  "\(baseUrl)?term=\(term)&entity=ibook"
        
        Alamofire.request(url).responseJSON {response in
            if response.response?.statusCode == 200 {
                do {
                    let result = try JSONDecoder().decode(ItunesResult.self, from: response.data!)
                    let books = result.results
                    completion(true, "Success", books)
                    return
                    
                } catch {
                    print("Erro ao decodificar")
                }
            }
            completion(false, msg, nil)
          
        }
    }
}

struct FailableDecodable<Base : Decodable> : Decodable {
    let base: Base?
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.base = try? container.decode(Base.self)
    }
}
