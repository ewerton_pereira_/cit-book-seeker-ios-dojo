//
//  BookDetailViewController.swift
//  BookSeeker
//
//  Created by Ewerton Pereira on 07/12/19.
//  Copyright © 2019 Ewerton Pereira. All rights reserved.
//

import UIKit

class BookDetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
        
    @IBOutlet var contentView:UIView!
    @IBOutlet var bookImage:UIImageView!
    @IBOutlet var lblAuthor:UILabel!
    @IBOutlet var lblPrice:UILabel!
    @IBOutlet var lblReleased:UILabel!
    @IBOutlet var textViewDescription:UITextView!
    @IBOutlet var textViewHeightConstraint:NSLayoutConstraint!
    
    var book:Book?
    var genres = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard book != nil else {
            let alert = UIAlertController(title: "Alert", message: "Please select another book", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
               self.navigationController?.popViewController(animated: true)
            }))

            self.present(alert, animated: true, completion: nil)
            return
        }
        
        bookImage.alpha = 0
        if let bookCoverUrl = URL(string: book!.artworkUrl100!) {
           
           bookImage.sd_setImage(with: bookCoverUrl, completed: {
               (image, error, cacheType, url) in
               
               UIView.animate(withDuration: 0.4, animations: {
                   self.bookImage.alpha = 1
               })
               
           })
           
        }
        
        lblAuthor.text = self.book?.artistName
        lblPrice.text = self.book?.formattedPrice
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd, yyyy"

        let date:Date? = dateFormatterGet.date(from: book!.releaseDate!)
        lblReleased.text = "Released in \(dateFormatterPrint.string(from: date!))"
        
        do {
            
            let str = try NSMutableAttributedString(data: book!.description!.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            let largeAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
            str.setAttributes(largeAttributes, range: NSRange(location: 0, length: str.string.count))
            
            self.textViewDescription.attributedText  = str
            
            let fixedWidth = self.textViewDescription.frame.size.width
            let newSize = self.textViewDescription.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            self.textViewDescription.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            textViewHeightConstraint.constant = newSize.height
            

        } catch {
            print("TextError")
        }
        
        
        self.title = book?.trackName
        
        if book?.genres != nil {
            self.genres = book!.genres!
        }

    }
    
    
    //A ideia era colocar botoes na selecao de categorias e fazer outra busca, ficaria legal...
    
    //MARK: Collection View Delegates
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        
        return 1
    }
    
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.genres.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GenreCell", for: indexPath) as! GenreCollectionViewCell
        
        cell.lblTitle.text = genres[indexPath.item]
        
        return cell
    }
    
    
}
