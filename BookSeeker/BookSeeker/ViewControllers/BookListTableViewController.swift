//
//  BookListTableViewController.swift
//  BookSeeker
//
//  Created by Ewerton Pereira on 07/12/19.
//  Copyright © 2019 Ewerton Pereira. All rights reserved.
//

import UIKit

class BookListTableViewController: UITableViewController {
    
    var searchTerm:String = ""
    var books = [Book]()
    var bookSelected:Book?

    override func viewDidLoad() {
        super.viewDidLoad()

        BookService.searchBookWithTerm(searchTerm, completion: {(success, msg, books) in
            
            if success && books != nil{
                guard books!.count > 0 else {
                    self.sendFailMessage("No results were found for your search.")
                    return
                }
                
                self.books = books!
                self.tableView.reloadData()
            }else{
                self.sendFailMessage(msg)
            }
            
        })
        
        self.title = searchTerm
    }
    
    func sendFailMessage(_ message:String){
        
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            self.navigationController?.popViewController(animated: true)
        }))
    
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        bookSelected = books[indexPath.row]
        return indexPath
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.books.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookCellIdentifier", for: indexPath) as! BookTableViewCell
        cell.setBook(books[indexPath.row])
        return cell
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookDetail" {
            let viewController = segue.destination as! BookDetailViewController
            viewController.book = bookSelected
        }
        
    }
    

}
