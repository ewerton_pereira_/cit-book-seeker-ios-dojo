//
//  SearchViewController.swift
//  BookSeeker
//
//  Created by Ewerton Pereira on 07/12/19.
//  Copyright © 2019 Ewerton Pereira. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var searchBar:UISearchBar!
    @IBOutlet var tableView:UITableView!
    
    var terms = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.terms = SearchDAO().list()
        self.tableView.reloadData()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookList" {
            let viewController = segue.destination as! BookListTableViewController
            viewController.searchTerm = self.searchBar.text!
            SearchDAO().add(term: self.searchBar.text!)
            
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "BookList" {
            if searchBar.text!.count < 3 {
                
                let alert = UIAlertController(title: "Alert", message: "Your search must be longer than 3 characters", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                        self.navigationController?.popViewController(animated: true)
                    }))
                
                self.present(alert, animated: true, completion: nil)
                
                return false
            }
        }
        
        return true
    }
    
    
    // MARK: - Table view delegate
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        searchBar.text = self.terms[indexPath.row]
        
        return indexPath
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.terms.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath)
        cell.textLabel?.text = self.terms[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Recent search"
    }

    
}
