//
//  BookTableViewCell.swift
//  BookSeeker
//
//  Created by Ewerton Pereira on 07/12/19.
//  Copyright © 2019 Ewerton Pereira. All rights reserved.
//

import UIKit
import SDWebImage

class BookTableViewCell: UITableViewCell {
    
    @IBOutlet var bookImage:UIImageView!
    @IBOutlet var lblBookTitle:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setBook (_ book:Book ){
        bookImage.alpha = 0
        if let bookCoverUrl = URL(string: book.artworkUrl60!) {
           
           bookImage.sd_setImage(with: bookCoverUrl, completed: {
               (image, error, cacheType, url) in
               
               UIView.animate(withDuration: 0.4, animations: {
                   self.bookImage.alpha = 1
               })
               
           })
           
        }

        lblBookTitle.text = book.trackName
    }

}
